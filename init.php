<?php
if (php_sapi_name() !== 'cli') { die("Файл должен быть запущен с коммандной строки!"); }

$xml = __DIR__ .'/hc.xml'; // физический адрес к XML файлу с комментариями HyperComments
$dir = dirname(__DIR__) .'/'; // корневая директория сайта

define('BASE_PATH', $dir);
define('WP_USE_THEMES', false);
global $wp, $wp_query, $wp_the_query, $wp_rewrite, $wp_did_header;
require(BASE_PATH . 'wp-load.php');

/**
 * Удаляем комментарии не относящиеся ни к одной записи
 */
$wpdb->query(
    'DELETE FROM `'. $wpdb->prefix .'comments` WHERE comment_post_ID = 0 OR comment_agent = \'HyperComments\''
);

/**
 * Получаем уже существующие комментарии
 */
$comments     = get_comments();
$arr_comments = array();
foreach ($comments as $comment) {
    $arr_comments[] = array( // формируем массив с необходимыми данными для проверки на существование
    	'id'   => $comment->comment_ID,
        'text' => $comment->comment_content,
        'date' => $comment->comment_date,
    );
}
unset($comments); // очищаем ненужное

/**
 * Парсим данные с файла и добавляем комментарий
 */
$content = file_get_contents($xml);
$result  = new SimpleXMLElement($content);
foreach ($result as $post) {
	if ($id = url_to_postid($post->url)) {
		foreach ($post->comments->comment as $comment) {
			$element_ID = array_search($comment->text, array_column($arr_comments, 'text'));
			$arr_time   = new DateTime($arr_comments[$element_ID]['date']);
			$comm_time  = new DateTime($comment->time);
			$isset      = ($element_ID ? ($arr_time->format('Y-m-d H:i') === $comm_time->modify('+3 hour')->format('Y-m-d H:i') ? true : false) : false);

			if (!$isset) {
				$user = get_user_by('email', $comment->email);
				$time = new DateTime($comment->time);
				// формируем комментарий
				$data = array(
					'user_id'              => $user->ID,
					'comment_post_ID'      => $id,
					'comment_author'       => $comment->nick,
					'comment_author_email' => $comment->email,
					'comment_content'      => $comment->text,
					'comment_author_IP'    => $comment->ip,
					'comment_date_gmt'     => $time->format('Y-m-d H:i:s'),
					'comment_date'         => $time->modify('+3 hour')->format('Y-m-d H:i:s'),
					'comment_agent'        => 'HyperComments',
				);
				wp_insert_comment(wp_slash($data));
			}
		}
	}
}

/**
 * Упорядычевание по дате
 */
$wpdb->query('CREATE TABLE `wp_comments_time` LIKE `'. $wpdb->prefix .'comments`;');
$wpdb->query('INSERT INTO `wp_comments_time` SELECT * FROM `'. $wpdb->prefix .'comments` ORDER BY comment_date;');
$wpdb->query('TRUNCATE `'. $wpdb->prefix .'comments`;');
$wpdb->query('INSERT INTO `'. $wpdb->prefix .'comments` SELECT * FROM `wp_comments_time`;');
$wpdb->query('DROP TABLE `wp_comments_time`;');


/*	 
	'CREATE TABLE `wp_comments_time` LIKE `'. $wpdb->prefix .'comments`;
	 INSERT INTO `wp_comments_time`
	 	SELECT * FROM `'. $wpdb->prefix .'comments` ORDER BY comment_date;
	 TRUNCATE `'. $wpdb->prefix .'comments`;
	 INSERT INTO `'. $wpdb->prefix .'comments`
	 	SELECT * FROM `wp_comments_time`;
	 DROP TABLE `wp_comments_time`;'
*/