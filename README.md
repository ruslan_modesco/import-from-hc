### Импорт комментариев HyperComments ###
## ЭТО ВАЖНО!
# на всякий случай делаем бекап таблицы с комментариями (wp_comments)

# Установка
1. распаковуем или копируем папку в корневую сайта
2. заменяем файл hc.xml (файл экспорта с HyperComments)
3. запускаем с коммандной строки файл init.php (php import-from-hc/init.php > import-from-hc/console.log)
4. проверяем колличество комментариев
5. если колличество комментариев приблизительно совпадает - удаляем папку скрипта, если нет читаем ниже

# Если количество комментариев напорядок больше:
1. заходим в таблицу wp_comments и смотрим разницу по часовым поясам между GMT и текущим сервера.
2. в файле init.php строки 53, 67 заменяем "->modify('+3 hour')" на разницу в часовых поясах

# вставленные ранее комментарии при новом запуске автоматически удаляются.
# вставленные комментарии в php my admin можно определить по полю comment_agent, в которое вставляется значение "HyperComments"